/**
 * Another binary tree implementation v1.1
 * Author: Raul Torres
 * 2020
 */

#pragma once

// includes
#include <vector>
#include <deque> // it allows pushing elements to the front

namespace binary_tree
{
    /**
     * Node class, it represents a node of the tree.
     * It keeps a value and has a vector for referencing the children nodes.
     */
    class Node
    {
        private: // valid for all instantiated nodes
            static const int maxChildren = 2;

        private: // members
            int mValue = 0;
            std::vector<Node*> mChildren;

        public: // methods
            Node(int pValue);
            void addChild(Node* child);
            void getPathToValue(int pValue, std::deque<int> &path) const;
    };
}
