/**
 * Another binary tree implementation v1.1
 * Author: Raul Torres
 * 2020
 */

// includes
#include <deque>
#include <iostream>
#include "binary_tree.h" // own header

// namespaces
using namespace std;
using namespace binary_tree;

/**
 * Print the values of a deque
 */
void printDeque(const deque<int> &path)
{
    cout << "Path: ";
    for(int value : path)
    {
        cout << value << " ";
    }
    cout << endl;
}

/**
 * Entry point
 */
int main()
{
    // create nodes with uniform initialization
    Node root{0};
    Node left{1};
    Node right{2};
    Node orphan{10};
    Node left_left{3};
    Node left_right{4};
    Node right_left{5};
    Node right_right{6};

    // connect nodes
    root.addChild(nullptr); // REJECTED!
    root.addChild(&left);
    root.addChild(&right);
    root.addChild(&orphan); // REJECTED!
    left.addChild(&left_left);
    left.addChild(&left_right);
    right.addChild(&right_left);
    right.addChild(&right_right);

    deque<int> path;

    // test 1
    root.getPathToValue(6, path);
    printDeque(path); // 0, 2, 6

    // test 2
    path.clear();
    root.getPathToValue(5, path);
    printDeque(path); // 0, 2, 5

    // test 3
    path.clear();
    root.getPathToValue(4, path);
    printDeque(path); // 0, 1, 4

    // test 5
    path.clear();
    root.getPathToValue(1, path);
    printDeque(path); // 0, 1

    // test 6
    path.clear();
    root.getPathToValue(7, path);
    printDeque(path); // nothing

    // bye!
    return 0;
}
