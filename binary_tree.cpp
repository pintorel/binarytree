/**
 * Another binary tree implementation v1.1
 * Author: Raul Torres
 * 2020
 */

// includes
#include <vector>
#include <deque> // it allows pushing elements to the front
#include <iostream>
#include "binary_tree.h" // own declarations


namespace binary_tree
{
    // namespaces
    using namespace std;

    /**
     * Simple Node constructor, initializes the value
     */
    Node::Node(int pValue):
        mValue{pValue} // uniform initialization
    {

    }

    /**
     * Add a child
     */
    void Node::addChild(Node* child)
    {
        // reject anomalies
        if(child == nullptr)
        {
            cout << "Cannot add a null pointer." << endl;
            return;
        }
        if(mChildren.size() >= maxChildren)
        {
            cout << "Cannot add more than " << maxChildren << " children to this node." << endl;
            return;
        }

        // accept child
        mChildren.push_back(child);
    }

    /**
     * Recursively find a value on the tree and report back the path to it
     */
    void Node::getPathToValue(int pValue, deque<int> &path) const // path is send by reference to avoid creating deque's at each recursion
    {
        // value found, push it to the deque
        if(mValue == pValue)
        {
            path.push_front(mValue);
            return;
        }

        // not found, traverse the children
        for(Node* child : mChildren)
        {
            child->getPathToValue(pValue, path); // recurse
            if(!path.empty()) // if found, update the path
            {
                path.push_front(mValue);
                return;
            }
        }

        // not found at all
        return;
    }
}
